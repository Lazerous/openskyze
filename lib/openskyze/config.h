#ifndef _SKYZE_CONFIG_H
#define _SKYZE_CONFIG_H

#include <stdlib.h>
#include <cyaml/cyaml.h>

#include <openskyze/common.h>

typedef struct
{
    gchar *name;
    int type; // SkyzeSourceType
    gboolean enable;
    gboolean loop;
    gboolean live_source;
    gboolean Intra_decode;
    guint smart_record;
    gint source_width;
    gint source_height;
    gint source_fps_n;
    gint source_fps_d;
    gint camera_csi_sensor_id;
    gint camera_v4l2_dev_node;
    gchar **uri;
    gint n_uri;
    gchar *dir_path;
    gchar *file_prefix;
    gint latency;
    gchar *smart_rec_file_prefix;
    gchar *smart_rec_dir_path;
    guint smart_rec_video_cache_size;
    guint smart_rec_container;
    guint smart_rec_def_duration;
    guint smart_rec_duration;
    guint smart_rec_start_time;
    guint smart_rec_interval;
    guint num_sources;
    guint gpu_id;
    guint camera_id;
    guint source_id;
    guint select_rtp_protocol;
    guint num_decode_surfaces;
    guint num_extra_surfaces;
    guint nvbuf_memory_type;
    guint cuda_memory_type;
    guint drop_frame_interval;
    gint rtsp_reconnect_interval_sec;
} skyze_config_source_t;

typedef struct {
    int enable_perf_measurement;
    int perf_measurement_interval_sec;
} app_t;

typedef struct {
    int enable;
    int gpu_id;
    int border_width;
    int text_size;
    const char *text_color;
    const char *text_bg_color;
    const char *font;
    int show_clock;
    int clock_x_offset;
    int clock_y_offset;
    int clock_text_size;
    const char *clock_color;
    int display_text;
    int display_bbox;
    int display_mask;
    int nvbuf_memory_type;
} osd_t;

typedef struct {
    int gpu_id;
    int live_source;
    int batch_size;
    int batched_push_timeout;
    int width;
    int height;
    int enable_padding;
    int nvbuf_memory_type;
} streammux_t;

typedef struct {
    int enable;
    int gpu_id;
    const char *labelfile_path;
    int batch_size;
    const char *bbox_border_color0;
    const char *bbox_border_color1;
    const char *bbox_border_color2;
    const char *bbox_border_color3;
    int interval;
    int gie_unique_id;
    int nvbuf_memory_type;
    const char *config_file;
    const char *model_file;
    const char *raw_output_directory;
    int plugin_type;
} skyze_config_inference_t;

typedef struct {
    int enable;
    int tracker_width;
    int tracker_height;
    const char *ll_lib_file;
    const char *ll_config_file;
    int gpu_id;
    int enable_batch_process;
    int display_tracking_id;
} tracker_t;

typedef struct {
    const char *name;
    int enable;
    int type;
    int sync;
    int source_id;
    int nvbuf_memory_type;
    int bitrate;
    const char *msg_conv_config;
    const char *msg_conv_payload_type;
    const char *msg_broker_proto_lib;
    const char *msg_broker_password;
    const char * msg_broker_hostname;
    const char *msg_broker_username;
    int msg_broker_port;
    const char *msg_broker_exchange;
    const char *msg_broker_topic;
    int msg_broker_share_connection;


    int container; //SkyzeContainerType container;
    int codec; //SkyzeEncoderType codec;
    int enc_type; //SkyzeEncHwSwType enc_type;
    guint profile;
    gchar *output_file_path;
    guint rtsp_port;
    guint udp_port;
    guint64 udp_buffer_size;
    guint iframeinterval;
    gint width;
    gint height;
    gboolean qos;
    gboolean qos_value_specified;
    guint gpu_id;
    guint display_id;
    guint overlay_id;
    guint offset_x;
    guint offset_y;
    /** MsgConv settings */
    gchar*    config_file_path;
    guint     conv_payload_type;
    gchar*    conv_msg2p_lib;
    guint     conv_comp_id;
    gchar*    debug_payload_dir;
    gboolean  multiple_payloads;
    /** Broker settings */
    gchar*    proto_lib;
    gchar*    conn_str;
    gchar*    topic;
    gchar*    broker_config_file_path;
    guint     broker_comp_id;
    gboolean  disable_msgconv;
    gboolean  new_api;

    // review
    gboolean link_to_demux;
} skyze_config_sink_t;

typedef struct {
    const char *name;
    int enable;
    const char *proto_lib;
    const char *conn_str;
    const char *config_file;
    const char **subscribe_topic;
    unsigned n_subscribe_topic;
} message_consumer_t;

typedef struct {
    app_t *app;
    osd_t *osd;
    streammux_t *streammux;
    skyze_config_inference_t *inference;
    tracker_t *tracker;
    skyze_config_source_t *sources;
    int sources_count;
    skyze_config_sink_t *sinks;
    int sinks_count;
    message_consumer_t *message_consumers;
    int message_consumers_count;
} skyze_settings_t;

typedef struct {
    cyaml_config_t yaml;
    cyaml_err_t err;
    skyze_settings_t *settings;
} skyze_config_t;

skyze_config_t *skyze_config_load(const char *config_uri);
int skyze_config_get_sources(skyze_config_t *config, skyze_config_source_t **sources, int *n_sources);
int skyze_config_get_sinks(skyze_config_t *config, skyze_config_sink_t **sinks, int *n_sinks);
int skyze_config_get_inference(skyze_config_t *config, skyze_config_inference_t **inference);
void skyze_config_free(skyze_config_t *config);

#endif
