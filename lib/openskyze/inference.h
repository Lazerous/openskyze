#ifndef _SKYZE_INFERENCE_H
#define _SKYZE_INFERENCE_H

#include <openskyze/openskyze.h>

typedef struct _skyze_inference_t skyze_inference_t;

skyze_inference_t *skyze_inference_new();
skyze_inference_t *skyze_inference_new_with_config(skyze_config_inference_t *inference);
GstElement *skyze_inference_get_bin(skyze_inference_t *inference);
GstPadProbeCallback skyze_inference_get_probe(skyze_inference_t *inference);
void skyze_inference_free();

#endif
