#ifndef _SKYZE_SINK_H
#define _SKYZE_SINK_H

#include <glib.h>

#include <openskyze/config.h>

typedef enum
{
  SKYZE_SINK_FAKE = 1,
  SKYZE_SINK_RENDER_EGL,
  SKYZE_SINK_ENCODE_FILE,
  SKYZE_SINK_UDPSINK,
  SKYZE_SINK_RENDER_OVERLAY,
  SKYZE_SINK_MSG_CONV_BROKER,
} SkyzeSinkType;

typedef enum
{
  SKYZE_CONTAINER_MP4 = 1,
  SKYZE_CONTAINER_MKV
} SkyzeContainerType;

typedef enum
{
  SKYZE_ENCODER_H264 = 1,
  SKYZE_ENCODER_H265,
  SKYZE_ENCODER_MPEG4
} SkyzeEncoderType;

typedef enum
{
  SKYZE_ENCODER_TYPE_HW,
  SKYZE_ENCODER_TYPE_SW
} SkyzeEncHwSwType;


typedef struct _skyze_sink_t skyze_sink_t;

skyze_sink_t *skyze_sink_new();
skyze_sink_t *skyze_sink_fake_new(skyze_config_sink_t *config);
skyze_sink_t *skyze_sink_egl_new(skyze_config_sink_t *config);
skyze_sink_t *skyze_sink_udp_new(skyze_config_sink_t *config);
skyze_sink_t *skyze_sink_file_new(skyze_config_sink_t *config);
skyze_sink_t *skyze_sink_overlay_new(skyze_config_sink_t *config);
void skyze_sink_free(skyze_sink_t *sink);

#endif
