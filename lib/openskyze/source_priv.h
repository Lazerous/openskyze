#ifndef _SKYZE_SOURCE_PRIV_H
#define _SKYZE_SOURCE_PRIV_H

#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#define MAX_SOURCE_BINS 10
#include <gstnvdsmeta.h>
#include <gst/rtsp/gstrtsptransport.h>

#include <nvdsgstutils.h>
#include <gst-nvdssr.h>

#include <openskyze/common.h>

#define SRC_CONFIG_KEY "src_config"
#define SOURCE_RESET_INTERVAL_SEC 60

struct _skyze_source_t {
  GstElement *bin;
  GstElement *src_elem;
  GstElement *cap_filter;
  GstElement *cap_filter1;
  GstElement *depay;
  GstElement *parser;
  GstElement *enc_que;
  GstElement *dec_que;
  GstElement *decodebin;
  GstElement *enc_filter;
  GstElement *encbin_que;
  GstElement *tee;
  GstElement *tee_rtsp_pre_decode;
  GstElement *fakesink_queue;
  GstElement *fakesink;
  GstElement *nvvidconv;
  gboolean do_record;
  guint64 pre_event_rec;
  GMutex bin_lock;
  guint bin_id;
  gint rtsp_reconnect_interval_sec;
  struct timeval last_buffer_time;
  struct timeval last_reconnect_time;
  gulong src_buffer_probe;
  gulong rtspsrc_monitor_probe;
  gpointer bbox_meta;
  GstBuffer *inbuf;
  gchar *location;
  gchar *file;
  gchar *direction;
  gint latency;
  gboolean got_key_frame;
  gboolean eskyze_done;
  gboolean reset_done;
  gboolean live_source;
  gboolean reconfiguring;
  gboolean async_state_watch_running;
  gulong probe_id;
  guint64 accumulated_base;
  guint64 prev_accumulated_base;
  guint source_id; 
  gpointer recordCtx;
  
  skyze_config_source_t *config;
  skyze_record_t *record;
};

typedef struct
{
  GstElement *bin;
  GstElement *streammux;
  GThread *reset_thread;
  //NvDsSrcBin sub_bins[MAX_SOURCE_BINS];
  guint num_bins;
  guint num_fr_on;
  gboolean live_source;
  gulong nvstreammux_eosmonitor_probe;
} NvDsSrcParentBin;

//static gboolean install_mux_eosmonitor_probe = FALSE;
//static gboolean reset_source_pipeline (gpointer data);
//static gboolean set_camera_csi_params (skyze_config_source_t * config, NvDsSrcBin * bin);
//static gboolean set_camera_v4l2_params (skyze_config_source_t * config, NvDsSrcBin * bin);
//static gboolean create_camera_source_bin (skyze_config_source_t * config, NvDsSrcBin * bin);
//static void cb_newpad (GstElement * decodebin, GstPad * pad, gpointer data);
//static void cb_sourcesetup (GstElement * object, GstElement * arg0, gpointer data);
//static gboolean seek_decode (gpointer data);
//static GstPadProbeReturn restart_stream_buf_prob (GstPad * pad, GstPadProbeInfo * info, gpointer u_data);
//static void decodebin_child_added (GstChildProxy * child_proxy, GObject * object, gchar * name, gpointer user_data);
//static void cb_newpad2 (GstElement * decodebin, GstPad * pad, gpointer data);
//static void cb_newpad3 (GstElement * decodebin, GstPad * pad, gpointer data);
//static gboolean cb_rtspsrc_select_stream (GstElement *rtspsrc, guint num, GstCaps *caps, gpointer user_data);
//void destroy_smart_record_bin (gpointer data);
//static gpointer smart_record_callback (NvDsSRRecordingInfo *info, gpointer userData);
//static gboolean smart_record_event_generator (gpointer data);
//static gboolean watch_source_status (gpointer data);
//static gboolean watch_source_async_state_change (gpointer data);
//static GstPadProbeReturn rtspsrc_monitor_probe_func (GstPad * pad, GstPadProbeInfo * info, gpointer u_data);
//static GstPadProbeReturn nvstreammux_eosmonitor_probe_func (GstPad * pad, GstPadProbeInfo * info, gpointer u_data);
//static gboolean create_rtsp_src_bin (skyze_config_source_t * config, NvDsSrcBin * bin);
//static gboolean create_uridecode_src_bin (skyze_config_source_t * config, NvDsSrcBin * bin);
gboolean create_source_bin (skyze_config_source_t * config, skyze_source_t* source);
gboolean create_multi_source_bin (guint num_sub_bins, skyze_config_source_t * configs, NvDsSrcParentBin * bin);
gboolean reset_source_pipeline (gpointer data);
gboolean set_source_to_playing (gpointer data);
gpointer reset_encodebin (gpointer data);

#endif
