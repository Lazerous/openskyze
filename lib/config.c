#include <stdlib.h>
#include <stdio.h>
#include <glib.h>

#include <cyaml/cyaml.h>

#include <openskyze/config.h>

static const cyaml_schema_value_t string_ptr_schema = {
	CYAML_VALUE_STRING(CYAML_FLAG_POINTER, char, 0, CYAML_UNLIMITED),
};

//static const cyaml_schema_value_t int_schema = {
//		CYAML_VALUE_INT(CYAML_FLAG_DEFAULT, int),
//};

static const cyaml_schema_field_t app_fields_schema[] = {
	CYAML_FIELD_INT(
			"enable-perf-measurement", CYAML_FLAG_OPTIONAL,
			app_t, enable_perf_measurement),
	CYAML_FIELD_INT(
			"perf-measurement-interval-sec", CYAML_FLAG_OPTIONAL,
			app_t, perf_measurement_interval_sec),
	CYAML_FIELD_END
};

static const cyaml_schema_field_t osd_fields_schema[] = {
	CYAML_FIELD_INT(
			"enable", CYAML_FLAG_OPTIONAL,
			osd_t, enable),
	CYAML_FIELD_INT(
			"gpu-id", CYAML_FLAG_OPTIONAL,
			osd_t, gpu_id),
	CYAML_FIELD_INT(
			"border-width", CYAML_FLAG_OPTIONAL,
			osd_t, border_width),
	CYAML_FIELD_INT( 
			"text-size", CYAML_FLAG_OPTIONAL,
			osd_t, text_size),
	CYAML_FIELD_STRING_PTR(
			"text-color", CYAML_FLAG_POINTER,
			osd_t, text_color, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"text-bg-color", CYAML_FLAG_POINTER,
			osd_t, text_bg_color, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"font", CYAML_FLAG_POINTER,
			osd_t, font, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"show-clock", CYAML_FLAG_OPTIONAL,
			osd_t, show_clock),
	CYAML_FIELD_INT(
			"clock-x-offset", CYAML_FLAG_OPTIONAL,
			osd_t, clock_x_offset),
	CYAML_FIELD_INT(
			"clock-y-offset", CYAML_FLAG_OPTIONAL,
			osd_t, clock_y_offset),
	CYAML_FIELD_INT(
			"clock-text-size", CYAML_FLAG_OPTIONAL,
			osd_t, clock_text_size),
	CYAML_FIELD_STRING_PTR(
			"clock-color", CYAML_FLAG_POINTER,
			osd_t, clock_color, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"display-text", CYAML_FLAG_OPTIONAL,
			osd_t, display_text),
	CYAML_FIELD_INT(
			"display-bbox", CYAML_FLAG_OPTIONAL,
			osd_t, display_bbox),
	CYAML_FIELD_INT(
			"display-mask", CYAML_FLAG_OPTIONAL,
			osd_t, display_mask),
	CYAML_FIELD_INT(
			"nvbuf-memory-type", CYAML_FLAG_OPTIONAL,
			osd_t, nvbuf_memory_type),
	CYAML_FIELD_END
};

static const cyaml_schema_field_t streammux_fields_schema[] = {
	CYAML_FIELD_INT(
			"gpu-id", CYAML_FLAG_OPTIONAL,
			streammux_t, gpu_id),
	CYAML_FIELD_INT(
			"live-source", CYAML_FLAG_OPTIONAL,
			streammux_t, live_source),
	CYAML_FIELD_INT(
			"batch-size", CYAML_FLAG_OPTIONAL,
			streammux_t, batch_size),
	CYAML_FIELD_INT(
			"batched-push-timeout", CYAML_FLAG_OPTIONAL,
			streammux_t, batched_push_timeout),
	CYAML_FIELD_INT(
			"width", CYAML_FLAG_OPTIONAL,
			streammux_t, width),
	CYAML_FIELD_INT(
			"height", CYAML_FLAG_OPTIONAL,
			streammux_t, height),
	CYAML_FIELD_INT(
			"enable-padding", CYAML_FLAG_OPTIONAL,
			streammux_t, enable_padding),
	CYAML_FIELD_INT(
			"nvbuf-memory-type", CYAML_FLAG_OPTIONAL,
			streammux_t, nvbuf_memory_type),
	CYAML_FIELD_END
};

static const cyaml_schema_field_t inference_fields_schema[] = {
	CYAML_FIELD_INT(
			"enable", CYAML_FLAG_OPTIONAL,
			skyze_config_inference_t, enable),
	CYAML_FIELD_INT(
			"gpu-id", CYAML_FLAG_OPTIONAL,
			skyze_config_inference_t, gpu_id),
	CYAML_FIELD_STRING_PTR(
			"labelfile-path", CYAML_FLAG_POINTER,
			skyze_config_inference_t, labelfile_path, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"batch-size", CYAML_FLAG_OPTIONAL,
			skyze_config_inference_t, batch_size),
	CYAML_FIELD_STRING_PTR(
			"bbox-border-color0", CYAML_FLAG_POINTER,
			skyze_config_inference_t, bbox_border_color0, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"bbox-border-color1", CYAML_FLAG_POINTER,
			skyze_config_inference_t, bbox_border_color1, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"bbox-border-color2", CYAML_FLAG_POINTER,
			skyze_config_inference_t, bbox_border_color2, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"bbox-border-color3", CYAML_FLAG_POINTER,
			skyze_config_inference_t, bbox_border_color3, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"interval", CYAML_FLAG_OPTIONAL,
			skyze_config_inference_t, interval),
	CYAML_FIELD_INT(
			"gie-unique-id", CYAML_FLAG_OPTIONAL,
			skyze_config_inference_t, gie_unique_id),
	CYAML_FIELD_INT(
			"nvbuf-memory-type", CYAML_FLAG_OPTIONAL,
			skyze_config_inference_t, nvbuf_memory_type),
	CYAML_FIELD_STRING_PTR(
			"config-file", CYAML_FLAG_POINTER,
			skyze_config_inference_t, config_file, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"model-file", CYAML_FLAG_POINTER,
			skyze_config_inference_t, model_file, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"raw-output-directory", CYAML_FLAG_POINTER,
			skyze_config_inference_t, raw_output_directory, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"plugin-type", CYAML_FLAG_OPTIONAL,
			skyze_config_inference_t, plugin_type),
	CYAML_FIELD_END
};

static const cyaml_schema_field_t tracker_fields_schema[] = {
	CYAML_FIELD_INT(
			"enable", CYAML_FLAG_OPTIONAL,
			tracker_t, enable),
	CYAML_FIELD_INT(
			"tracker-width", CYAML_FLAG_OPTIONAL,
			tracker_t, tracker_width),
	CYAML_FIELD_INT(
			"tracker-height", CYAML_FLAG_OPTIONAL,
			tracker_t, tracker_height),
	CYAML_FIELD_STRING_PTR(
			"ll-lib-file", CYAML_FLAG_POINTER,
			tracker_t, ll_lib_file, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"ll-config-file", CYAML_FLAG_POINTER,
			tracker_t, ll_config_file, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"gpu-id", CYAML_FLAG_OPTIONAL,
			tracker_t, gpu_id),
	CYAML_FIELD_INT(
			"enable-batch-process", CYAML_FLAG_OPTIONAL,
			tracker_t, enable_batch_process),
	CYAML_FIELD_INT(
			"display-tracking-id", CYAML_FLAG_OPTIONAL,
			tracker_t, display_tracking_id),

	CYAML_FIELD_END
};

static const cyaml_schema_field_t source_fields_schema[] = {
	CYAML_FIELD_STRING_PTR(
			"name", CYAML_FLAG_POINTER,
			skyze_config_source_t, name, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"enable", CYAML_FLAG_OPTIONAL,
			skyze_config_source_t, enable),
	CYAML_FIELD_INT(
			"gpu-id", CYAML_FLAG_OPTIONAL,
			skyze_config_source_t, gpu_id),
	CYAML_FIELD_INT(
			"nvbuf-memory-type", CYAML_FLAG_OPTIONAL,
			skyze_config_source_t, nvbuf_memory_type),
	CYAML_FIELD_INT(
			"type", CYAML_FLAG_OPTIONAL,
			skyze_config_source_t, type),
	CYAML_FIELD_SEQUENCE_COUNT(
			"uri", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_source_t, uri, n_uri,
			&string_ptr_schema, 0, CYAML_UNLIMITED), 
	CYAML_FIELD_STRING_PTR(
			"smart-record", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_source_t, smart_record, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"smart-rec-container", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_source_t, smart_rec_container, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"smart-rec-file-prefix", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_source_t, smart_rec_file_prefix, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"smart-rec-dir-path", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_source_t, smart_rec_dir_path, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"smart-rec-video-cache", CYAML_FLAG_OPTIONAL,
			skyze_config_source_t, smart_rec_video_cache_size),
	CYAML_FIELD_INT(
			"smart-rec-default-duration", CYAML_FLAG_OPTIONAL,
			skyze_config_source_t, smart_rec_def_duration),
	CYAML_FIELD_INT(
			"smart-rec-start-time", CYAML_FLAG_OPTIONAL,
			skyze_config_source_t, smart_rec_start_time),
	CYAML_FIELD_INT(
			"smart-rec-interval", CYAML_FLAG_OPTIONAL,
			skyze_config_source_t, smart_rec_interval),
	CYAML_FIELD_END
};

static const cyaml_schema_value_t source_schema = {
	CYAML_VALUE_MAPPING(CYAML_FLAG_DEFAULT,
			skyze_config_source_t, source_fields_schema),
};

static const cyaml_schema_field_t sink_fields_schema[] = {
	CYAML_FIELD_STRING_PTR(
			"name", CYAML_FLAG_POINTER,
			skyze_config_sink_t, name, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"enable", CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, enable),
	CYAML_FIELD_INT(
			"gpu-id", CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, gpu_id),
	CYAML_FIELD_INT(
			"nvbuf-memory-type", CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, nvbuf_memory_type),
	CYAML_FIELD_STRING_PTR(
			"type", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, type, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"sync", CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, sync),
	CYAML_FIELD_INT(
			"source-id", CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, source_id),
	CYAML_FIELD_STRING_PTR(
			"codec", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, codec, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"enc-type", CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, enc_type),
	CYAML_FIELD_INT(
			"bitrate", CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, bitrate),
	CYAML_FIELD_INT(
			"profile", CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, profile),
	CYAML_FIELD_INT(
			"rtsp-port", CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, rtsp_port),
	CYAML_FIELD_INT(
			"udp-port", CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, udp_port),
	CYAML_FIELD_STRING_PTR(
			"msg-conv-config", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, msg_conv_config, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"msg-conv-payload-type", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, msg_conv_payload_type, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"msg-broker-proto-lib", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, msg_broker_proto_lib, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"msg-broker-password", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, msg_broker_password, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"msg-broker-hostname", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, msg_broker_hostname, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"msg-broker-username", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, msg_broker_username, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"msg-broker-port", CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, msg_broker_port),
	CYAML_FIELD_STRING_PTR(
			"msg-broker-exchange", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, msg_broker_exchange, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"msg-broker-topic", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, msg_broker_topic, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"msg-broker-share-connection", CYAML_FLAG_OPTIONAL,
			skyze_config_sink_t, msg_broker_share_connection),
	CYAML_FIELD_END
};

static const cyaml_schema_field_t message_consumer_fields_schema[] = {
	CYAML_FIELD_STRING_PTR(
			"name", CYAML_FLAG_POINTER,
			message_consumer_t, name, 0, CYAML_UNLIMITED),
	CYAML_FIELD_INT(
			"enable", CYAML_FLAG_OPTIONAL,
			message_consumer_t, enable),
	CYAML_FIELD_STRING_PTR(
			"proto-lib", CYAML_FLAG_POINTER,
			message_consumer_t, proto_lib, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"conn-str", CYAML_FLAG_POINTER,
			message_consumer_t, conn_str, 0, CYAML_UNLIMITED),
	CYAML_FIELD_STRING_PTR(
			"config-file", CYAML_FLAG_POINTER,
			message_consumer_t, config_file, 0, CYAML_UNLIMITED),
	CYAML_FIELD_SEQUENCE_COUNT(
			"subscribe-topic", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			message_consumer_t, subscribe_topic, n_subscribe_topic,
			&string_ptr_schema, 0, CYAML_UNLIMITED), 
	CYAML_FIELD_END
};

static const cyaml_schema_value_t message_consumer_schema = {
	CYAML_VALUE_MAPPING(CYAML_FLAG_DEFAULT,
			message_consumer_t, message_consumer_fields_schema),
};

static const cyaml_schema_value_t sink_schema = {
	CYAML_VALUE_MAPPING(CYAML_FLAG_DEFAULT,
			skyze_config_sink_t, sink_fields_schema),
};

static const cyaml_schema_field_t settings_fields_schema[] = {
	CYAML_FIELD_MAPPING_PTR(
			"application", CYAML_FLAG_DEFAULT,
			skyze_settings_t, app, app_fields_schema),
	CYAML_FIELD_MAPPING_PTR(
			"osd", CYAML_FLAG_DEFAULT,
			skyze_settings_t, osd, osd_fields_schema),
	CYAML_FIELD_MAPPING_PTR(
			"streammux", CYAML_FLAG_DEFAULT,
			skyze_settings_t, streammux, streammux_fields_schema),
	CYAML_FIELD_MAPPING_PTR(
			"inference", CYAML_FLAG_DEFAULT,
			skyze_settings_t, inference, inference_fields_schema),
	CYAML_FIELD_MAPPING_PTR(
			"tracker", CYAML_FLAG_DEFAULT,
			skyze_settings_t, tracker, tracker_fields_schema),
	CYAML_FIELD_SEQUENCE(
			"sources", CYAML_FLAG_POINTER,
			skyze_settings_t, sources, 
			&source_schema, 0, CYAML_UNLIMITED),
	CYAML_FIELD_SEQUENCE(
			"sinks", CYAML_FLAG_POINTER,
			skyze_settings_t, sinks, 
			&sink_schema, 0, CYAML_UNLIMITED),
	CYAML_FIELD_SEQUENCE(
			"message-consumers", CYAML_FLAG_POINTER | CYAML_FLAG_OPTIONAL,
			skyze_settings_t, message_consumers, 
			&message_consumer_schema, 0, CYAML_UNLIMITED),
	CYAML_FIELD_END
};

static const cyaml_schema_value_t settings_schema = {
	CYAML_VALUE_MAPPING(CYAML_FLAG_POINTER,
			skyze_settings_t, settings_fields_schema),
};

skyze_config_t* skyze_config_load(const char *config_uri) {
	int rc = SKYZE_SUCCESS;
	skyze_config_t *config = NULL;

	if (config_uri == NULL) {
		rc = SKYZE_INVALID_URI;
		goto exit;
	}

	config = g_try_new0(skyze_config_t, 1);

	if (config == NULL) {
		rc = SKYZE_NO_MEM;
		goto exit;
	}

	config->yaml.log_level = CYAML_LOG_WARNING;
	config->yaml.log_fn = cyaml_log;
	config->yaml.mem_fn = cyaml_mem;
	config->yaml.flags = CYAML_CFG_IGNORE_UNKNOWN_KEYS;

	config->err = cyaml_load_file(config_uri, &config->yaml, &settings_schema, (void **) &config->settings, NULL);

	if (config->err != CYAML_OK) {
		rc = SKYZE_INVALID_CONFIG;
		goto exit;
	}
exit:
	if (rc != SKYZE_SUCCESS) {
		if (config) {
			skyze_config_free(config);
			config = NULL;
		}
	}

	return config;
}

int skyze_config_get_sources(skyze_config_t *config, skyze_config_source_t **sources, int *n_sources)
{
	int rc = SKYZE_SUCCESS;

	if (config == NULL || sources == NULL || n_sources == NULL) {
		rc = SKYZE_INVALID_ARGS;
		goto exit;
	}

	*sources = config->settings->sources;
	*n_sources = config->settings->sources_count;

exit:
	return rc;
}

int skyze_config_get_sinks(skyze_config_t *config, skyze_config_sink_t **sinks, int *n_sinks)
{
	int rc = SKYZE_SUCCESS;

	if (config == NULL || sinks == NULL || n_sinks == NULL) {
		rc = SKYZE_INVALID_ARGS;
		goto exit;
	}

	*sinks = config->settings->sinks;
	*n_sinks = config->settings->sinks_count;

exit:
	return rc;
}

int skyze_config_get_inference(skyze_config_t *config, skyze_config_inference_t **inference)
{
	int rc = SKYZE_SUCCESS;

	if (config == NULL || inference == NULL) {
		rc = SKYZE_INVALID_ARGS;
		goto exit;
	}

	*inference = config->settings->inference;
exit:
	return rc;
}

void skyze_config_free(skyze_config_t *config) {
	if (config) {
		//cyaml_free(&config->yaml, &settings_schema, config->settings, 0);
		cyaml_free(&config->yaml, NULL, NULL, 0);
		g_free(config);
		config = NULL;
	}
}
