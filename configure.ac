AC_INIT([openskyze], [0.1.0], [steve@klumzy.com])
AM_INIT_AUTOMAKE([-Wall -Werror foreign])
AC_PROG_CC
AC_PROG_CXX
AM_PROG_AR
LT_INIT([shared static])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_MACRO_DIR([m4])
ACLOCAL_AMFLAGS="-I m4"

AX_CHECK_CUDA

AX_VALGRIND_DFLT([sgcheck],[off])
AX_VALGRIND_DFLT([drd],[off])
AX_VALGRIND_DFLT([helgrind],[off])
AX_VALGRIND_CHECK

AC_ARG_ENABLE([tegra],
[  --enable-tegra    Enable platform support for Tegra],
[case "${enableval}" in
  yes) OPENSKYZE_CFLAGS="-DPLATFORM_TEGRA " ;;
  no)  OPENSKYZE_CFLAGS="" ;;
  *) AC_MSG_ERROR([bad value ${enableval} for --enable-tegra]) ;;
esac],[tegra=false])
AM_CONDITIONAL([TEGRA], [test x$tegra = xtrue])

AC_ARG_ENABLE([debug],
[  --enable-debug    Enable debugging support],
[case "${enableval}" in
  yes) OPENSKYZE_CFLAGS+="-ggdb3 -O0 " ;;
  no)  OPENSKYZE_CFLAGS+="-O2 " ;;
  *) AC_MSG_ERROR([bad value ${enableval} for --enable-debug]) ;;
esac],[debug=false])
AM_CONDITIONAL([DEBUG], [test x$debug = xtrue])

PKG_CHECK_MODULES(GLIB, glib-2.0 >= 2.6)
PKG_CHECK_MODULES(CYAML, libcyaml >= 1.1)
PKG_CHECK_MODULES(GST, gstreamer-1.0)
PKG_CHECK_MODULES(GST_VIDEO, gstreamer-video-1.0)
PKG_CHECK_MODULES(X11, x11)

DEEPSTREAM_CFLAGS="\
 $GST_CFLAGS \
 $GST_VIDEO_CFLAGS \
 $X11_CFLAGS \
 -I/opt/nvidia/deepstream/deepstream/include \
 -I/opt/nvidia/deepstream/deepstream/sources/includes"

DEEPSTREAM_LIBS="\
 $GST_LIBS \
 $GST_VIDEO_LIBS \
 $X11_LIBS \
 -L/opt/nvidia/deepstream/deepstream/lib \
 -Wl,-rpath,-L/opt/nvidia/deepstream/deepstream/lib \
 -lnvdsgst_smartrecord \
 -lnvds_utils \
 -lnvdsgst_meta \
 -lnvdsgst_helper"

AC_SUBST([GLIB_CFLAGS])
AC_SUBST([GLIB_LIBS])
AC_SUBST([DEEPSTREAM_CFLAGS])
AC_SUBST([DEEPSTREAM_LIBS])

OPENSKYZE_CFLAGS+=" \
 -std=gnu99 \
 -Wall \
 -Wextra \
 -Werror \
 -Wshadow \
 -Wno-unused \
 -Wdouble-promotion \
 -Wformat=2 \
 -Wformat-truncation \
 -Werror=implicit-function-declaration \
 -Wno-deprecated-declarations \
 -Werror=format-security \
 -Werror=array-bounds \
 -ffunction-sections \
 -fdata-sections \
 -fstack-protector-all \
 -fexceptions \
 -pipe \
 -D_FORTIFY_SOURCE=2 \
 -Wl,-pie \
 -Wl,-z,defs \
 -Wl,-z,now \
 -Wl,-z,relro \
 -Wl,--gc-sections "

OPENSKYZE_CXXFLAGS+=" \
 -Wall \
 -Wextra \
 -Werror \
 -Wshadow \
 -Wno-unused \
 -Wdouble-promotion \
 -Wformat=2 \
 -Wformat-truncation \
 -Wno-deprecated-declarations \
 -Werror=format-security \
 -Wno-missing-field-initializers \
 -Werror=array-bounds \
 -ffunction-sections \
 -fdata-sections \
 -fstack-protector-all \
 -fexceptions \
 -pipe \
 -D_FORTIFY_SOURCE=2 \
 -Wl,-z,now \
 -Wl,-z,relro \
 -Wl,--gc-sections "


OPENSKYZE_CFLAGS+=$GLIB_CFLAGS
OPENSKYZE_LIBS=$GLIB_LIBS

AC_SUBST([OPENSKYZE_LIBS])
AC_SUBST([OPENSKYZE_CFLAGS])
AC_SUBST([OPENSKYZE_CXXFLAGS])

AC_CONFIG_FILES([
 Makefile
 src/Makefile
 tests/Makefile
 lib/Makefile
 plugins/Makefile
 plugins/inference/Makefile
 plugins/inference/yolo/Makefile
])
AC_OUTPUT
