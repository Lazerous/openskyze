![OpenSkyze, built with and for NVIDIA](https://openskyze.com/images/openskyze_builtwith.png)

**The AI powered video analytics platform powered by NVIDIA Deepstream.**


OpenSkyze is a scalable video processing platform powered by NVIDIA Deepstream.  OpenSkyze aims to provide easily composable video processing pipelines to enable real-time video processing and AI capabilities.  OpenSkyze will provide capabilities that can be run at the edge on populat devices such as the NVIDIA Jetson Platorm (Nano, Xavier NX, Xavier AGX, etc) and on high-end servers with NVIDIA's descrete GPUs (dGPU).

OpenSkyze goal is to provde production level drop-in capability for cloud and edge to devices that will enable video processing, analytics, and machine learning capabilities that will implement into your existing capabilities.  In addition to this easy-to-use capability, we aim to provide the interfaces and APIs to allow experts to easily extend and expand the features provided by our platform.


# Status
OpenSkyze is currently pre-alpha and under heavy development.  The latest develop build can be checkout out from the master branch and the latest containers can be pulled from our [container registry](https://gitlab.com/openskyze/openskyze/container_registry).

# Development Setup

This currently assumes you already have CUDA and Deepstream already installed and working properly with your NVIDIA GPU or Jetson Platform.  The documentation will be expanded to cover these items in the future.

## Package Requirements
```
apt install libglib2.0-dev make git yamllint clang-tidy clang-tools-10 libyaml-dev
git clone https://github.com/tlsa/libcyaml.git
cd libcyaml
make && sudo make install
```

## NVIDIA Deepstream Setup
```
I will add documentation to setup deepstream and the repos
```

### Add Deepstream to ld.so.conf

Create the file /etc/ld.so.conf.d/deepstream.conf, and add the following contents:
```
/opt/nvidia/deepstream/deepstream-5.0/lib/
```


## Getting the code
```
git clone git@gitlab.com:openskyze/openskyze.git
cp hooks/pre-commit .git/hooks/pre-commit
chmod +x .git/hooks/pre-commit
```

## Pull the build container
```
docker pull registry.gitlab.com/openskyze/openskyze/openskyze-build:latest
```

## Build the build container
```
docker build -t registry.gitlab.com/openskyze/openskyze/openskyze-build -f Dockerfile.build.amd64 .
```

## Known warnings
This warning will occur on x86_64 containers.  This can be ignored and is harmless.  nvinferserver can't be used since it is not installed on the x86 dGPU platform.
```
(deepstream-app:16632): GStreamer-WARNING **: 13:13:31.201: Failed to load plugin '/usr/lib/x86_64-linux-gnu/gstreamer-1.0/deepstream/libnvdsgst_inferserver.so': libtrtserver.so
```

# Is OpenSkyze the right choice for me?
* Do you need object detection, classification, and segmentation for video streams and files?
* Do you have data from lots of different sources, video files, rtsp, v4l2, usb, etc?
* Do you care about high-speed video processing and analytics capabilities?
* Do you need a scalable video surveilance system with AI capabilities?
* Do you need a capability that enables Air Domain Airwareness with the ability to detect drones, planes, and other aircraft?

# Developers
* We are currently looking for developers that have skills and interest in:
  * NVIDIA Deepstream in C
  * RabbitMQ development in C
  * Gstreamer Development
  * Implementing unit tests, and integration tests.
  * Software Q&A and Testing
  * Documentation
* You can join our Development channel on [Discord](https://discord.gg/J7Cbmde6hW)


# Contributing
**Use Issues to track everything**

## Developers
- For bug fixes, small updates, please fork and send a Merge Request.
- For larger changes and new features, please open an issue for discussion before submitting a Merge Request.
- All merge requests should include the following:
  - Statement to license the code under the MIT License.
  - Proper test-cases to test the code.
  - Documentation
  - Code Example

## Testing and Documentation
- You can also contribute by:
  - Testing and reporting issues.  Please be as detailed as possible.
  - Suggesting new features or enhancements.  New features and enhancement should have solid use-cases and justification why the feature should be added.
  - Improve and update the documentation

# Contact
* Follow us on Twitter at [@OpenSkyze](http://twitter.com/OpenSkyze)


# Stay In Touch

- [Twitter](https://twitter.com/openskyze)
- [Website](https://openskyze.com)
* [Discord](https://discord.gg/J7Cbmde6hW)


# License

[MIT](http://opensource.org/licenses/MIT)

Copyright (c) 2020-present, Steve McDaniel


# Running the prototype

This is being phased out and only serves as a working example of deepstream.

## Building Notes
```
docker run --gpus all -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -v $HOME/src/openskyze:/opt/nvidia/deepstream/deepstream/sources/apps/sample_apps -e DISPLAY=$DISPLAY -w /opt/nvidia/deepstream/deepstream/sources/apps/sample_apps  nvcr.io/nvidia/deepstream:5.0.1-20.09-devel make
```

## Developing on Xavier
```
cd /opt/nvidia/deepstream/deepstream/sources/apps/sample_apps
git clone git@gitlab.com:openskyze/openskyze.git
cd openskyze
make 
./openskyze -c configs/default.txt
```

## Building yolo
```
cd /opt/nvidia/deepstream/deepstream-5.0/sources/objectDetector_Yolo/nvdsinfer_custom_impl_Yolo
CUDA_VER=10.2 make -j
```


## Starting RabbitMQ
OpenSkyze will publish all of the analytics via AMQP.  You'll need to launch the server before starting openskyze.  The management UI will be available at: http://localhost:15672/
```
docker run -d --network host --hostname rabbit --name rabbit -e RABBITMQ_DEFAULT_USER=guest -e RABBITMQ_DEFAULT_PASS=guest rabbitmq:3-management
```

